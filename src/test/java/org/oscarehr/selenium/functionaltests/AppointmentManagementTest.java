/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.functionaltests;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.*;
import org.oscarehr.selenium.pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.ConnectionConfig;
import org.oscarehr.selenium.support.ScreenshotRule;

/**
 * This class tests the functionality of appointments such as creation, editing, and deletion.
 */
public class AppointmentManagementTest {
	private static Logger log = Logger.getLogger(AppointmentManagementTest.class);
	private AppointmentAccessPage appointmentAccess;
	private static WebDriver driver;
	
	/**
	 * initialize driver and profile
	 * @throws Exception
	 */
	@BeforeClass
	public static void prepareConfig() throws Exception {
		//Only use this if you are not running the master suite
		//Config.load("config.xml");
		//ConnectionConfig.load("connection_config.xml");
		log.info("Starting AppointmentManagementTest...");
		FirefoxProfile profile = new FirefoxProfile();
		profile.setAssumeUntrustedCertificateIssuer(true);
		profile.setPreference("dom.max_script_run_time", 100);
		driver = new FirefoxDriver(profile);
		driver.manage().timeouts().implicitlyWait(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
	}
	
	/**
	 * initailizes the screenshot rule
	 */
	@Rule
    public ScreenshotRule screenshotRule = new ScreenshotRule(driver);
	
	/**
	 * Loads necessary page objects in order to get to navigate to the page required for the tests
	 * @throws Exception
	 */
	@Before
	public void prepareComponents() throws Exception {
		LoginPage loginTest = new LoginPage(driver);
		appointmentAccess = new AppointmentAccessPage(driver, loginTest);
	}
	  
	/**
	 * quits the driver
	 */
	@AfterClass
	public static void tearDownComponents() {
		driver.quit();
	}

	/**
	 * Attempts to create an appointment
	 */
	@Test
	public void appointmentCreationTest() {
		log.info("AppointmentManagementTest.appointmentCreationTest------------------------------------------------------");
		AppointmentCreationPage appointmentCreationTest = new AppointmentCreationPage(driver, appointmentAccess);
		appointmentCreationTest.get();
		assertTrue("Appointment Creation Test failed: failed to create appointment", appointmentCreationTest.createAppointment(Config.getAppointmentOneDetails()));
	}
	
	/**
	 * Attempts to edit an existing appointment
	 */
	@Test
	public void appointmentEditTest() {
		log.info("AppointmentManagementTest.appointmentEditTest------------------------------------------------------");
		AppointmentManagementPage appointmentEditTest = new AppointmentManagementPage(driver, appointmentAccess);
		appointmentEditTest.get();
		assertTrue("Appointment Edit Test failed: failed to edit appointment", appointmentEditTest.editAppointment(Config.getAppointmentTwoDetails()));
	}
	
	/**
	 * Attempts to delete an existing appointment
	 */
	@Test
	public void appointmentDeletionTest() {
		log.info("AppointmentManagementTest.appointmentDeletionTest--------------------------------------------");
		AppointmentManagementPage appointmentDeletionTest = new AppointmentManagementPage(driver, appointmentAccess);
		appointmentDeletionTest.get();
		assertTrue("Appointment Creation Test failed: failed to delete appointment", appointmentDeletionTest.deleteAppointment());
	}
}
