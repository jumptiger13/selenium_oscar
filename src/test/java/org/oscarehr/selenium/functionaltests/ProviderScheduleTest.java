/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.functionaltests;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.*;
import org.oscarehr.selenium.pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.ConnectionConfig;
import org.oscarehr.selenium.support.ScreenshotRule;

/**
 * This class tests the functionality of provider scheduling and attempts to create, assign, and delete schedules
 */
public class ProviderScheduleTest {
	private static Logger log = Logger.getLogger(ProviderScheduleTest.class);
	private ScheduleSettingMenuPage scheduleSetting;
	private static WebDriver driver;
	
	/**
	 * Loads necessary page objects in order to get to navigate to the page required for the tests
	 * @throws Exception
	 */
	@BeforeClass
	public static void prepareConfig() throws Exception {
		//Only use this if you are not running the master suite
		//Config.load("config.xml");
		//ConnectionConfig.load("connection_config.xml");
		log.info("Starting ProviderScheduleTest...");
		FirefoxProfile profile = new FirefoxProfile();
		profile.setAssumeUntrustedCertificateIssuer(true);
		profile.setPreference("dom.max_script_run_time", 100);
		driver = new FirefoxDriver(profile);
		driver.manage().timeouts().implicitlyWait(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
	}

	/**
	 * initailizes the screenshot rule
	 */
	@Rule
	public ScreenshotRule screenshotRule = new ScreenshotRule(driver);
	
	/**
	 * Loads necessary page objects in order to get to navigate to the page required for the tests
	 * @throws Exception
	 */
	@Before
	public void prepareComponents() throws Exception {
		LoginPage loginTest = new LoginPage(driver);
		AppointmentAccessPage appointmentAccess = new AppointmentAccessPage(driver, loginTest);
		AdminMenuPage adminMenu = new AdminMenuPage(driver, appointmentAccess);
		scheduleSetting = new ScheduleSettingMenuPage(driver, adminMenu);
		driver.manage().timeouts().implicitlyWait(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
	}
	  
	/**
	 * quits the driver
	 */
	@AfterClass
	public static void tearDownComponents() {
		driver.quit();
	}
	
	/**
	 * Attempts to create a new schedule for a provider
	 */
	@Test
	public void createScheduleTemplateTest() {
		log.info("ProviderScheduleTest.createScheduleTest------------------------------------------------------");
		TemplateSettingPage createScheduleTemplateTest = new TemplateSettingPage(driver, scheduleSetting);
		createScheduleTemplateTest.get();
		assertTrue("Create a Provider Record Test failed: failed to create a new provider", createScheduleTemplateTest.createUserTemplateSetting(Config.getScheduleSettings()));
	}
	
	/**
	 * Attempts to assign a schedule to a provider
	 */
	@Test
	public void scheduleProviderTest() {
		log.info("ProviderScheduleTest.scheduleProviderTest------------------------------------------------------");
		ScheduleProviderPage scheduleProviderTest = new ScheduleProviderPage(driver, scheduleSetting);
		scheduleProviderTest.get();
		assertTrue("Schedule a Provider Test failed: failed to schedule a provider, the schedule setting may not exist", scheduleProviderTest.scheduleProvider(Config.getScheduleSettings()));
	}
	
	/**
	 * Attempts to delete a schedule off a scheduled provider
	 */
	@Test
	public void deleteScheduleTest() {
		log.info("ProviderScheduleTest.deleteScheduleTest--------------------------------------------");
		ScheduleProviderPage deleteScheduleTest = new ScheduleProviderPage(driver, scheduleSetting);
		deleteScheduleTest.get();
		assertTrue("Deleting a Schedule Test failed: failed to delete schedule, the schedule may not exist", deleteScheduleTest.deleteSchedule(Config.getScheduleSettings()));
	}
}
