/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.functionaltests;

import static org.junit.Assert.*;
import static org.junit.Assume.*;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.*;
import org.openqa.selenium.firefox.*;
import org.oscarehr.selenium.pages.*;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.ConnectionConfig;
import org.oscarehr.selenium.support.ScreenshotRule;
import org.openqa.selenium.WebDriver;

/**
 * This class checks the mandatory functionalities of a patient demographic such as creating and editing
 */
public class PatientDemographicTest {
	private static Logger log = Logger.getLogger(PatientDemographicTest.class);
	private AppointmentAccessPage appointmentAccess;
	private static WebDriver driver;
	
	/**
	 * Loads necessary page objects in order to get to navigate to the page required for the tests
	 * @throws Exception
	 */
	@BeforeClass
	public static void prepareConfig() throws Exception {
		//Only use this if you are not running the master suite
		//Config.load("config.xml");
		//ConnectionConfig.load("connection_config.xml");
		log.info("Starting PatientDemographicTest...");
		FirefoxProfile profile = new FirefoxProfile();
		profile.setAssumeUntrustedCertificateIssuer(true);
		profile.setPreference("dom.max_script_run_time", 100);
		driver = new FirefoxDriver(profile);
		driver.manage().timeouts().implicitlyWait(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
	}

	/**
	 * initailizes the screenshot rule
	 */
	@Rule
	public ScreenshotRule screenshotRule = new ScreenshotRule(driver);
	
	/**
	 * Loads necessary page objects in order to get to navigate to the page required for the tests
	 * @throws Exception
	 */
	@Before
	public void prepareComponent() throws Exception {
		LoginPage loginTest = new LoginPage(driver);
		appointmentAccess = new AppointmentAccessPage(driver, loginTest);
	}
	  
	/**
	 * quits the driver
	 */
	@AfterClass
	public static void tearDownComponents() {
		driver.quit();
	}
	
	/**
	 * Attempts to create a patient if one does not already exist
	 */
	@Test
	public void patientCreationTest() {
		log.info("PatientDemographicTest.patientCreationTest------------------------------------------------------");
		PatientSearchPage checkPatient = new PatientSearchPage(driver, appointmentAccess);
		checkPatient.get();
		assumeTrue(!checkPatient.checkPatient(Config.getPatientOneDemographicDetails().getLastName()));
		PatientDemographicCreationPage patientCreationTest = new PatientDemographicCreationPage(driver, checkPatient);
		patientCreationTest.get();
		assertTrue("Patient Creation Test failed: Patient may already exist", patientCreationTest.createPatientDemographic(Config.getPatientOneDemographicDetails()));
	}
	
	/**
	 * Attempts to edit an existing patient
	 */
	@Test
	public void patientDemographicEditTest() {
		log.info("PatientDemographicTest.patientDemographicEditTest------------------------------------------------------");
		PatientSearchPage patientSearch = new PatientSearchPage(driver, appointmentAccess);
		PatientDemographicPage patientDemographicEditTest = new PatientDemographicPage(driver, patientSearch);
		patientDemographicEditTest.get();
		assertTrue("Patient Demographic Edit Test failsed: Unable to edit patient demographic information", patientDemographicEditTest.editDemographic(Config.getPatientOneDemographicDetails()));
	}
}
