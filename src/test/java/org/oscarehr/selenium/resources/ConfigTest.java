/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.resources;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * This class is used to test the Config class and it's load and save functionalities
 */
public class ConfigTest {
	
	/**
	 * Set new values for login credentials
	 */
	@BeforeClass
	public static void setUp(){
		Config.setGoodCredentials(new LoginCredentials("oscardoc", "mac2002", "1117"));
		Config.setBadCredentials(new LoginCredentials("test", "test", "test"));
		Config.setNoCredentials(new LoginCredentials("","",""));
	}
	
	/**
	 * Attempt to save the config to an external XML file
	 */
	@Test
	public void testSave() {
		System.out.println("Save");
		Config conf = new Config();
		try{
			assertTrue(conf.save("test.xml"));
		} catch (Exception ex){
			fail("Exception occurred. "+ ex.getMessage());
		}
	}
	
	/**
	 * Attempt to load the external config
	 */
	@Test
	public void testLoad(){
		System.out.println("Load");
		try {
			assertNotNull(Config.load("test.xml"));
		} catch (Exception e) {
			fail("Exception occurred: "+e.getMessage());
		}
		
		
	}

}
