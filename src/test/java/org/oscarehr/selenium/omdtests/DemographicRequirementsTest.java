/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.omdtests;

import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.junit.*;
import org.openqa.selenium.firefox.*;
import org.oscarehr.selenium.pages.*;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.ConnectionConfig;
import org.oscarehr.selenium.support.ScreenshotRule;
import org.openqa.selenium.WebDriver;

/**
 * This class tests the OMD Requirements of the patient demographic
 */
public class DemographicRequirementsTest {
	private static Logger log = Logger.getLogger(DemographicRequirementsTest.class);
	private AppointmentAccessPage appointmentAccess;
	private static WebDriver driver;
	
	/**
	 * initialize driver and profile
	 * @throws Exception
	 */
	@BeforeClass
	public static void prepareConfig() throws Exception {
		//Only use this if you are not running the master suite
		Config.load("config.xml");
		ConnectionConfig.load("connection_config.xml");
		log.info("Starting DemographicRequirementsTest...");
		FirefoxProfile profile = new FirefoxProfile();
		profile.setAssumeUntrustedCertificateIssuer(true);
		profile.setPreference("dom.max_script_run_time", 100);
		driver = new FirefoxDriver(profile);
		driver.manage().timeouts().implicitlyWait(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
	}
	
	/**
	 * initailizes the screenshot rule
	 */
	@Rule
    public ScreenshotRule screenshotRule = new ScreenshotRule(driver);
	
	/**
	 * Loads necessary page objects in order to get to navigate to the page required for the tests
	 * @throws Exception
	 */
	@Before
	public void prepareComponents() throws Exception {
		LoginPage loginTest = new LoginPage(driver);
		appointmentAccess = new AppointmentAccessPage(driver, loginTest);
	}
	
	/**
	 * quits the driver
	 */
	@AfterClass
	public static void tearDownComponents() {
		driver.quit();
	}
	
	/**
	 * Verifies that all required OMD contact data is in the Patient Demographic Screen
	 */
	@Test
	public void OMDFieldRequirementsTest() {
		log.info("DemographicRequirementsTest.OMDFieldRequirementsTest------------------------------------------------------");
		PatientSearchPage patientSearch = new PatientSearchPage(driver, appointmentAccess);
		PatientDemographicPage patientDemographic = new PatientDemographicPage(driver, patientSearch);
		patientDemographic.get();
		patientDemographic.omdFieldRequirements();
	}
	
	/**
	 * Verifies that Enrollment History works for a patient demographic
	 */
	@Test
	public void enrollmentHistoryTest() {
		log.info("DemographicRequirementsTest.enrollmentHistoryTest------------------------------------------------------");
		PatientSearchPage patientSearch = new PatientSearchPage(driver, appointmentAccess);
		PatientDemographicPage patientDemographic = new PatientDemographicPage(driver, patientSearch);
		EnrollmentHistoryPage enrollmentHistoryTest = new EnrollmentHistoryPage(driver, patientDemographic);
		enrollmentHistoryTest.get();
	}
	
	/**
	 * Verifies that Appointment History works for a patient demographic
	 */
	@Test
	public void appointmentHistoryTest() {
		log.info("DemographicRequirementsTest.appointmentHistoryTest------------------------------------------------------");
		PatientSearchPage patientSearch = new PatientSearchPage(driver, appointmentAccess);
		PatientDemographicPage patientDemographic = new PatientDemographicPage(driver, patientSearch);
		AppointmentHistoryPage appointmentHistoryTest = new AppointmentHistoryPage(driver, patientDemographic);
		appointmentHistoryTest.get();
	}
	
	/**
	 * Checks if the Drugref Interaction Preference is visible on the demographic screen
	 */
	@Test
	public void drugrefInteractionTest() {
		log.info("DemographicRequirementsTest.drugrefInteractionTest------------------------------------------------------");
		PatientSearchPage patientSearch = new PatientSearchPage(driver, appointmentAccess);
		PatientDemographicPage patientDemographic = new PatientDemographicPage(driver, patientSearch);
		patientDemographic.get();
		patientDemographic.checkDrugrefInteractionPreference();
	}
	
	/**
	 * Searches for a patient by health card number
	 */
	@Test
	public void searchHealthCardTest() {
		log.info("DemographicRequirementsTest.searchHealthCardTest------------------------------------------------------");
		PatientSearchPage patientSearch = new PatientSearchPage(driver, appointmentAccess);
		patientSearch.get();
		patientSearch.patientHealthCardSearch(Config.getPatientOneDemographicDetails().getHin());
	}
	
	/**
	 * Adds a relation to a patient demographic
	 */
	@Test
	public void addRelationTest() {
		log.info("DemographicRequirementsTest.addRelationTest------------------------------------------------------");
		PatientSearchPage patientSearch = new PatientSearchPage(driver, appointmentAccess);
		PatientDemographicPage patientDemographic = new PatientDemographicPage(driver, patientSearch);
		PatientRelationPage addRelationTest = new PatientRelationPage(driver, patientDemographic);
		addRelationTest.get();
		addRelationTest.addRelation(Config.getPatientOneDemographicDetails().getLastName());
	}
}
