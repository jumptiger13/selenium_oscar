/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.selenium.omdtests;

import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.junit.*;
import org.openqa.selenium.firefox.*;
import org.oscarehr.selenium.pages.*;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.ConnectionConfig;
import org.oscarehr.selenium.resources.PageUtils;
import org.oscarehr.selenium.support.ScreenshotRule;
import org.openqa.selenium.WebDriver;

public class PrescriptionRequirementsTest {
	private static Logger log = Logger.getLogger(PrescriptionRequirementsTest.class);
	private PatientSearchPage patientSearch;
	private static WebDriver driver;
	
	/**
	 * initialize driver and profile
	 * @throws Exception
	 */
	@BeforeClass
	public static void prepareConfig() throws Exception {
		//Only use this if you are not running the master suite
		//Config.load("config.xml");
		//ConnectionConfig.load("connection_config.xml");
		log.info("Starting PrescriptionRequirementsTest...");
		FirefoxProfile profile = new FirefoxProfile();
		profile.setAssumeUntrustedCertificateIssuer(true);
		profile.setPreference("dom.max_script_run_time", 100);
		driver = new FirefoxDriver(profile);
		driver.manage().timeouts().implicitlyWait(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(ConnectionConfig.getPageTimeout(), TimeUnit.SECONDS);
	}
	
	/**
	 * initailizes the screenshot rule
	 */
	@Rule
    public ScreenshotRule screenshotRule = new ScreenshotRule(driver);
	
	/**
	 * Loads necessary page objects in order to get to navigate to the page required for the tests
	 * @throws Exception
	 */
	@Before
	public void prepareComponents() throws Exception {
		LoginPage loginTest = new LoginPage(driver);
		AppointmentAccessPage appointmentAccess = new AppointmentAccessPage(driver, loginTest);
		patientSearch = new PatientSearchPage(driver, appointmentAccess);
		
	}
	
	/**
	 * quits the driver
	 */
	@AfterClass
	public static void tearDownComponents() {
		PageUtils.logout(driver);
		driver.quit();
	}
	
	/**
	 * This function tests the ability to view the date of the drugref database
	 */
	@Test
	public void drugrefInformationTest() {
		PatientRxPage drugrefInformationTest = new PatientRxPage(driver, patientSearch);
		drugrefInformationTest.get();
		drugrefInformationTest.drugrefInfo();
	}
	
	/**
	 * This function tests the ability to check for drug-to-drug/drug-to-allergy interaction information
	 */
	@Test
	public void drugInteractionTest() {
		PatientRxPage drugInteractionTest = new PatientRxPage(driver, patientSearch);
		drugInteractionTest.get();
		drugInteractionTest.checkDrugInteraction();
	}
	
	/**
	 * This function tests the ability to record (and identify) a medication prescribed by an external provider
	 */
	@Ignore
	@Test
	public void externalProviderMedicationTest() {
		
	}
	
	/**
	 * This function tests the ability to discontinue a medication from the treatment plan
	 */
	@Test
	public void discontinueMedicationTest() {
		PatientRxPage discontinueMedicationTest = new PatientRxPage(driver, patientSearch);
		discontinueMedicationTest.get();
		discontinueMedicationTest.discontinueMedication();
	}
	
	/**
	 * This function tests the ability to add a custom drug (ie for compound script)
	 */
	@Test
	public void addCustomDrugTest() {
		PatientRxPage addCustomDrugTest = new PatientRxPage(driver, patientSearch);
		addCustomDrugTest.get();
		addCustomDrugTest.addCustomDrug();
	}
	
	/**
	 * This function tests the ability to create a list of favorite prescriptions
	 */
	@Ignore
	@Test
	public void favoritePrescriptionFromMedicationViewTest() {
		PatientRxPage patientRx = new PatientRxPage(driver, patientSearch);
		MedicationViewPage favoritePrescriptionFromMedicationViewTest = new MedicationViewPage(driver, patientRx);
		favoritePrescriptionFromMedicationViewTest.get();
		favoritePrescriptionFromMedicationViewTest.addFavoriteMedication();		
	}
	
	/**
	 * This function tests the ability to create a list of favorite prescriptions
	 */
	@Ignore
	@Test
	public void favoritePrescriptionFromRxPrescriptionTest() {
		PatientRxPage favoritePrescriptionFromRxPrescriptionTest = new PatientRxPage(driver, patientSearch);
		favoritePrescriptionFromRxPrescriptionTest.get();
		favoritePrescriptionFromRxPrescriptionTest.addFavoriteMedication();
	}
	
	/**
	 * This function tests the ability to enable/disable drug display on the eChart Rx box
	 */
	@Test
	public void eChartRxDisplayTest() {
		PatientRxPage eChartRxDisplayTest = new PatientRxPage(driver, patientSearch);
		eChartRxDisplayTest.get();
		eChartRxDisplayTest.hideCPPDisplay();
	}
	
	/**
	 * This function tests the ability to add over-the-counter meds such as herbal or nutritional supplements
	 */
	@Test
	public void addOtherMedicationTest() {
		PatientEChartPage addOtherMedicationTest = new PatientEChartPage(driver, patientSearch);
		addOtherMedicationTest.get();
		addOtherMedicationTest.addOtherMedication();
	}
	
	/**
	 * This function tests the OMD required fields and options for the Rx Page
	 */
	@Ignore
	@Test
	public void requiredOMDFieldsTest() {
		
	}
}