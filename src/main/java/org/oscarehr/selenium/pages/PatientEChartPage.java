/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.*;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.PageUtils;

/**
 * The Patient EChart Page. Appointment Access -> Search -> Patient EChart
 * The associated JSP page is /casemgmt/forward.jsp
 */
public class PatientEChartPage extends LoadableComponent<PatientEChartPage> {
	private static Logger log = Logger.getLogger(PatientEChartPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public PatientEChartPage(WebDriver driver, PatientSearchPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempts to add a note to the patient's EChart
	 * @param noteContent The contents of the note
	 * @return The success of the addition
	 * @throws InterruptedException 
	 */
	public boolean addNote(String noteContent) {		
		log.info("Running method addNote...");
		driver.findElement(By.id("newNoteImg")).click();
		
		PageUtils.confirmAlertWindow(driver);
		PageUtils.waitForElementToEnable(By.cssSelector("div.newNote div textarea.txtArea"), driver);
		//Selenium 2's sendKeys command has issues focusing on an element so we use a click to force focus on the element
		driver.findElement(By.cssSelector("div.newNote div textarea.txtArea")).click();
		driver.findElement(By.cssSelector("div.newNote div textarea.txtArea")).sendKeys(noteContent);
		
		driver.findElement(By.id("observationDate_cal")).click();
		driver.findElement(By.cssSelector("div.calendar td.today")).click();
		
		//TODO: Would like to put this back in, but the current way this select box works is unreliable
		//driver.findElement(By.cssSelector("input[id^='encTypeSelect']")).click();
		//driver.findElement(By.cssSelector("li[id*='face to face encounter with client']")).click();
		
		driver.findElement(By.id("signVerifyImg")).click();
		
		PageUtils.switchWindows("PATIENT SEARCH", driver);
		
		return true;
	}
	
	/**
	 * Verify that a note with the note contents exist on the patient's EChart
	 * @param noteContent The contents of the note
	 * @return The existence of the note
	 */
	public boolean verifyNote(String noteContent) {
		log.info("Running method verifyNote...");
		if (driver.findElement(By.cssSelector("div.note div[id^='n'] div[id^='txt']")).getText().contains(noteContent)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Attempts to add a medication into the Other Medication category
	 * @return successfully added a medication under the Other Medication category
	 */
	public boolean addOtherMedication() {
		log.info("Running method addOtherMedication...");
		driver.findElement(By.cssSelector("div#OMeds div h3 a")).click();
		
		PageUtils.waitForElementToLoad(By.id("noteEditTxt"), driver);
		driver.findElement(By.id("noteEditTxt")).sendKeys("TEST DRUG");
		driver.findElement(By.id("startdate")).sendKeys("2013-05-01");
		driver.findElement(By.id("resolutiondate")).sendKeys("2013-05-01");
		driver.findElement(By.xpath("/html/body/div[5]/form/span/input[4]")).click();
		return true;
	}
	
	/**
	 * Navigates to the Prevention Page
	 * @return page loaded
	 */
	public boolean preventionPage() {
		log.info("running method preventionPage...");
		driver.findElement(By.partialLinkText("Preventions")).click();
		
		PageUtils.switchWindows("Preventions", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/oscarPrevention/index"));
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading PatientEChartPage...");
		parent.get();
		((PatientSearchPage)parent).patientEChartSearch(Config.getPatientOneDemographicDetails().getLastName());
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if PatientEChartPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Patient EChart Page failed to load: URL does not match", url.contains("/casemgmt/forward"));
		PageUtils.confirmAlertWindow(driver);
		PageUtils.waitForElementToLoad(By.id("menuTitlepreventions"), driver);
	}
}
