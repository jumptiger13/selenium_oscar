package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.oscarehr.selenium.resources.PageUtils;

public class AppointmentSearchPage extends LoadableComponent<AppointmentSearchPage> {
	private static Logger log = Logger.getLogger(AppointmentSearchPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public AppointmentSearchPage(WebDriver driver, AppointmentAccessPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	public boolean appointmentSearch() {
		Select select = new Select(driver.findElement(By.name("provider_no")));
		select.selectByVisibleText("Welby, Marcus");
		
		select = new Select(driver.findElement(By.name("dayOfWeek")));
		select.selectByVisibleText("Any Weekday");
		
		select = new Select(driver.findElement(By.name("startTime")));
		select.selectByVisibleText("9 am");
		
		select = new Select(driver.findElement(By.name("endTime")));
		select.selectByVisibleText("5 pm");
		
		select = new Select(driver.findElement(By.name("code")));
		select.selectByVisibleText("Any");
		
		select = new Select(driver.findElement(By.name("numberOfResults")));
		select.selectByVisibleText("3");
		
		driver.findElement(By.xpath("/html/body/form/table/tbody/tr[6]/td/input[2]")).submit();
		
		PageUtils.switchWindows("SEARCH RESULTS", driver);
	    String url = driver.getCurrentUrl();
	    return (url.contains("/appointment/appointmentsearch"));
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading appointmentSearchPage...");
		parent.get();
		((AppointmentAccessPage) parent).appointmentSearchPage();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if AppointmentSearchPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Appointment Search Page failed to load: URL does not match", url.contains("/appointment/appointmentsearch"));
		PageUtils.waitForElementToLoad(By.name("provider_no"), driver);
	}
}

