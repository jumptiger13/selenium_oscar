package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.oscarehr.selenium.resources.PageUtils;

public class AppointmentHistoryPage extends LoadableComponent<AppointmentHistoryPage> {
	private static Logger log = Logger.getLogger(AppointmentHistoryPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public AppointmentHistoryPage(WebDriver driver, PatientDemographicPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading AppointmentHistoryPage...");
		parent.get();
		((PatientDemographicPage) parent).appointmentHistoryPage();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if AppointmentHistoryPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Appointment History Page failed to load: URL does not match", url.contains("displaymode=appt_history"));
		PageUtils.waitForElementToLoad(By.partialLinkText("Back"), driver);
	}
}
