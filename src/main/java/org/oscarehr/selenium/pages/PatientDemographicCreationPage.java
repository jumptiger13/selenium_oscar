package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.PageUtils;
import org.oscarehr.selenium.resources.PatientDemographicDetails;

/**
 * The Patient Demographic Creation Page. Appointment Access -> Search -> Create Demographic
 * The associated JSP page is /demographic/demographicaddarecordhtm.jsp
 */
public class PatientDemographicCreationPage extends LoadableComponent<PatientDemographicCreationPage> {
	private static Logger log = Logger.getLogger(PatientDemographicCreationPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public PatientDemographicCreationPage(WebDriver driver, PatientSearchPage parent) {
		this.driver = driver;
		this.parent = parent;
	}

	/**
	 * Creates a new demographic
	 * @return page loaded
	 */
	public boolean createPatientDemographic(PatientDemographicDetails demoDetails) {
		log.info("Running method createPatientDemographic...");
		driver.findElement(By.name("first_name")).sendKeys(demoDetails.getFirstName());
		driver.findElement(By.name("year_of_birth")).clear();
		driver.findElement(By.name("year_of_birth")).sendKeys(demoDetails.getYearOfBirth());
		    
		driver.findElement(By.id("btnAddRecord")).submit();
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading PatientDemographicCreationPage...");
		parent.get();
		((PatientSearchPage)parent).createPatientDemographicPage(Config.getPatientOneDemographicDetails().getLastName());
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if PatientDemographicCreationPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Patient Demographic Page failed to load: URL does not match", url.contains("/demographic/demographicaddarecordhtm"));
		PageUtils.waitForElementToContainValue(By.name("year_of_birth"),"yyyy", driver);
	}
}
