package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.oscarehr.selenium.resources.PageUtils;

public class PatientRelationPage extends LoadableComponent<PatientRelationPage> {
	private static Logger log = Logger.getLogger(PatientRelationPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
		
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public PatientRelationPage(WebDriver driver, PatientDemographicPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * This method adds a relation to an existing patient's demographic
	 * @return success of adding a relation
	 */
	public boolean addRelation(String lastName) {
		driver.findElement(By.name("Submit")).submit();
		
		PageUtils.switchWindows("demographicsearch", driver);
    	PageUtils.waitForElementToLoad(By.name("keyword"), driver);
		
    	driver.findElement(By.name("keyword")).clear();
	    driver.findElement(By.name("keyword")).sendKeys(lastName);
	    driver.findElement(By.name("displaymode")).submit();
	    
	    PageUtils.waitForElementToLoad(By.cssSelector("td.demoId input.mbttn"), driver);
    	driver.findElement(By.cssSelector("td.demoId input.mbttn")).click();
    	
    	PageUtils.switchWindows("Add Relation", driver);
    	PageUtils.waitForElementToLoad(By.name("Submit"), driver);
    	
    	Select select = new Select(driver.findElement(By.name("relation")));
		select.selectByValue("Father");
		
		driver.findElement(By.name("sdm")).click();
		driver.findElement(By.name("emergContact")).click();
		
		driver.findElement(By.name("notes")).sendKeys("This is a test");
		driver.findElement(By.cssSelector("div.prevention fieldset label input")).submit();
		
		return true;
	}
		
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading PatientRelationPage...");
		parent.get();
		((PatientDemographicPage) parent).relationPage();
	}
		
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if PatientRelationPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Enrollment History Page failed to load: URL does not match", url.contains("/demographic/AddAlternateContact"));
		PageUtils.waitForElementToLoad(By.name("Submit"), driver);
	}
}
