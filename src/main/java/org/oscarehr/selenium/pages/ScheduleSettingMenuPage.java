/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.oscarehr.selenium.resources.PageUtils;

/**
 * The Schedule Setting Page. Appointment Access -> Admin -> Schedule Setting
 * The associated JSP page is /schedule/scheduletemplatesetting.jsp
 */
public class ScheduleSettingMenuPage extends LoadableComponent<ScheduleSettingMenuPage> {
	private static Logger log = Logger.getLogger(ScheduleSettingMenuPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public ScheduleSettingMenuPage(WebDriver driver, AdminMenuPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Navigates to the associated provider's template setting page
	 * @param providerId the provider's Id
	 * @return page loaded
	 */
	public boolean templateSettingPage(String providerId) {
		log.info("Running method userTemplateSetting...");
		Select select = new Select(driver.findElement(By.name("providerid")));
		select.selectByVisibleText(providerId);
		
		driver.findElement(By.partialLinkText("Template Setting")).click();
		
		PageUtils.switchWindows("DAY TEMPLATE SETTING", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/schedule/scheduleedittemplate"));
	}
	
	/**
	 * Navigates to the schedule provider page of the associated provider
	 * @param providerId the provider's Id
	 * @return page loaded
	 */
	public boolean scheduleProviderPage(String providerId) {
		log.info("Running method scheduleProvider...");		
		Select select = new Select(driver.findElement(By.name("provider_no")));
		select.selectByVisibleText(providerId);
		
		PageUtils.switchWindows("SCHEDULE SETTING", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/schedule/scheduletemplateapplying"));
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading ScheduleSettingMenuPage...");
		parent.get();
		((AdminMenuPage)parent).scheduleSetting();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if ScheduleSettingMenuPage is loaded...");
	    String url = driver.getCurrentUrl();
	    assertTrue("Schedule Setting Home Page failed to load: URL does not match", url.contains("/schedule/scheduletemplatesetting"));
	    PageUtils.waitForElementToLoad(By.name("Button"), driver);
	}
}