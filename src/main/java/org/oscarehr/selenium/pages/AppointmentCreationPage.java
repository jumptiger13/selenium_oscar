/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.oscarehr.selenium.resources.AppointmentDetails;
import org.oscarehr.selenium.resources.PageUtils;

/**
 * The Appointment Creation Page. Appointment Access -> Appointment Creation
 * The associated JSP page is /appointment/addappointment.jsp
 */
public class AppointmentCreationPage extends LoadableComponent<AppointmentCreationPage> {
	private static Logger log = Logger.getLogger(AppointmentCreationPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public AppointmentCreationPage(WebDriver driver, AppointmentAccessPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Creates an appointment with the associated appointment details
	 * @param appDetails The Appointment Details of the appointment to be created
	 * @return The success of the creation
	 */
	public boolean createAppointment(AppointmentDetails appDetails) {
		log.info("Running method createAppointment...");
    	driver.findElement(By.name("searchBtn")).click();
    	
    	PageUtils.switchWindows("Patient Search", driver);
    	PageUtils.waitForElementToLoad(By.name("keyword"), driver);
			    
	    driver.findElement(By.name("keyword")).sendKeys(appDetails.getLastName());
	    driver.findElement(By.name("displaymode")).submit();
	    
	    PageUtils.waitForElementToLoad(By.cssSelector("td.demoId input.mbttn"), driver);
    	driver.findElement(By.cssSelector("td.demoId input.mbttn")).click();
    	
    	PageUtils.switchWindows("ADD APPOINTMENT", driver);
    	PageUtils.waitForElementToLoad(By.name("reason"), driver);
    	//Fill in reason, notes, location, and resources with test data and submit the appointment creation
    	driver.findElement(By.name("reason")).sendKeys(appDetails.getReason());
    	driver.findElement(By.name("notes")).sendKeys(appDetails.getNotes());
    	driver.findElement(By.name("location")).sendKeys(appDetails.getLocation());
    	driver.findElement(By.name("resources")).sendKeys(appDetails.getResources());
    	driver.findElement(By.id("addButton")).click();
    	
    	return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading AppointmentCreationPage...");
		parent.get();
		((AppointmentAccessPage)parent).appointmentCreationPage();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if AppointmentCreationPage is loaded...");
	    String url = driver.getCurrentUrl();
	    assertTrue("Appointment Creation Page failed to load: URL does not match", url.contains("/appointment/addappointment"));
	    PageUtils.waitForElementToLoad(By.name("searchBtn"), driver);
	}
}
