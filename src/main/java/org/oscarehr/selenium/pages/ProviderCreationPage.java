/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.oscarehr.selenium.resources.PageUtils;
import org.oscarehr.selenium.resources.ProviderDetails;

/**
 * The Provider Creation Page. Appointment Access -> Admin -> Create Provider Record
 * The associated JSP page is /admin/provideraddarecordhtm.jsp
 */
public class ProviderCreationPage extends LoadableComponent<ProviderCreationPage> {
	private static Logger log = Logger.getLogger(ProviderCreationPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public ProviderCreationPage(WebDriver driver, AdminMenuPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempt to create a provider record with the associated provider details
	 * @param providerDetails The provider record details
	 * @return The success of the creation
	 */
	public boolean createProviderRecord(ProviderDetails providerDetails) {
		log.info("Running method createProviderRecord...");
		//TODO: change this By statement when a name/id is assigned to it
		driver.findElement(By.xpath("/html/body/center/form/table/tbody/tr/td[2]/input[2]")).click();
		driver.findElement(By.name("last_name")).sendKeys(providerDetails.getLastName());
		driver.findElement(By.name("first_name")).sendKeys(providerDetails.getFirstName());
		
		Select select = new Select(driver.findElement(By.name("provider_type")));
		select.selectByVisibleText(providerDetails.getProviderType());
		
		driver.findElement(By.name("ohip_no")).sendKeys(providerDetails.getOhipNo());
		
		driver.findElement(By.name("submitbtn")).click();
    	return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading ProviderCreationPage...");
		parent.get();
		((AdminMenuPage)parent).createProviderRecord();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if ProviderCreationPage is loaded...");
	    String url = driver.getCurrentUrl();
	    assertTrue("Admin Menu Page failed to load: URL does not match", url.contains("/admin/provideraddarecordhtm"));
	    PageUtils.waitForElementToLoad(By.name("submitbtn"), driver);
	}
}
