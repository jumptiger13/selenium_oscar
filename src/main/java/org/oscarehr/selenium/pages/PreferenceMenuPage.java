/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.oscarehr.selenium.resources.PageUtils;

public class PreferenceMenuPage extends LoadableComponent<PreferenceMenuPage> {
	private static Logger log = Logger.getLogger(PreferenceMenuPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public PreferenceMenuPage(WebDriver driver, AppointmentAccessPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempts to set the Rx interaction warning level
	 * @return successfully set the Rx Interaction Warning Level
	 */
	public boolean setRxInteractionLevel() {
		log.info("Running method setRxInteractionLevel...");
		Select select = new Select(driver.findElement(By.id("rxInteractionWarningLevel")));
		select.selectByVisibleText("Medium");
		
		driver.findElement(By.cssSelector("form div input")).click();
		return true;
	}
	
	/**
	 * Navigates to the Configure eChart CPP page
	 * @return page loaded
	 */
	public boolean eChartCPPPage() {
		log.info("running method eChartCPPPage...");
		driver.findElement(By.partialLinkText("eChart CPP")).click();
		
		PageUtils.switchWindows("eChart CPP", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("provider/CppPreferences"));
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading PreferenceMenuPage...");
		parent.get();
		((AppointmentAccessPage) parent).preferencePage();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if PreferenceMenuPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Preference Menu Page failed to load: URL does not match", url.contains("/provider/providerpreference"));
		PageUtils.waitForElementToLoad(By.id("rxInteractionWarningLevel"), driver);
	}
}