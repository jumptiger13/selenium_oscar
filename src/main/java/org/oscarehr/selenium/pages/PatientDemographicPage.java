/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.PageUtils;
import org.oscarehr.selenium.resources.PatientDemographicDetails;

/**
 * The Patient Demographic Page. Appointment Access -> Search -> Patient Demographic
 * The associated JSP page is /demographic/demographiccontrol.jsp
 */
public class PatientDemographicPage extends LoadableComponent<PatientDemographicPage> {
	private static Logger log = Logger.getLogger(PatientDemographicPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public PatientDemographicPage(WebDriver driver, PatientSearchPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Edit the specified patient with the information associated with the patient demographic details
	 * @param patientDemo The patient demographic details to be modified
	 * @return The success of the modification
	 */
	public boolean editDemographic(PatientDemographicDetails patientDemo) {
		log.info("Running method editDemographic...");
		String[] date = null;
		driver.findElement(By.id("editBtn")).click();
		
		PageUtils.waitForElementToLoad(By.name("hin"), driver);
		driver.findElement(By.name("hin")).sendKeys(patientDemo.getHin());
		driver.findElement(By.name("ver")).sendKeys(patientDemo.getVer());

		date = patientDemo.getEff_date().split("-");
		driver.findElement(By.name("eff_date_year")).sendKeys(date[0]);
		driver.findElement(By.name("eff_date_month")).sendKeys(date[1]);
		driver.findElement(By.name("eff_date_date")).sendKeys(date[2]);
		date = null;
		
		date = patientDemo.getHc_renew_date().split("-");
		driver.findElement(By.name("hc_renew_date_year")).sendKeys(date[0]);
		driver.findElement(By.name("hc_renew_date_month")).sendKeys(date[1]);
		driver.findElement(By.name("hc_renew_date_date")).sendKeys(date[2]);
		date = null;
		
		Select select = new Select(driver.findElement(By.name("provider_no")));
		select.selectByVisibleText(patientDemo.getProvider());
		
		select = new Select(driver.findElement(By.name("roster_status")));
		select.selectByValue(patientDemo.getRosterStatus());
		
		date = patientDemo.getRoster_date().split("-");
		driver.findElement(By.name("roster_date_year")).sendKeys(date[0]);
		driver.findElement(By.name("roster_date_month")).sendKeys(date[1]);
		driver.findElement(By.name("roster_date_day")).sendKeys(date[2]);
		date = null;
		
		driver.findElement(By.cssSelector("span#updateButton input")).submit();
		
		return true;
	}
	
	/**
	 * Navigates to the Enrollment History Page
	 * @return page loaded
	 */
	public boolean enrollmentHistoryPage() {
		log.info("Running method enrollmentHistoryPage...");
		driver.findElement(By.partialLinkText("Enrollment History")).click();
		
		PageUtils.switchWindows("Enrollment History", driver);
	    String url = driver.getCurrentUrl();
	    return (url.contains("/demographic/EnrollmentHistory"));
	}
	
	/**
	 * Navigates to the Appointment History Page
	 * @return page loaded
	 */
	public boolean appointmentHistoryPage() {
		log.info("Running method appointmentHistoryPage...");
		driver.findElement(By.partialLinkText("Appt.History")).click();
		
		PageUtils.switchWindows("APPOINTMENT HISTORY", driver);
	    String url = driver.getCurrentUrl();
	    return (url.contains("/demographic/demographiccontrol"));
	}
	
	/**
	 * Navigates to the Relation Page
	 * @return page loaded
	 */
	public boolean relationPage() {
		log.info("Running method relationPage...");
		driver.findElement(By.partialLinkText("Add Relation")).click();
		
		PageUtils.switchWindows("Add Relation", driver);
	    String url = driver.getCurrentUrl();
	    return (url.contains("/demographic/AddAlternateContact"));
	}
	
	/**
	 * Checks if the Drugref interaction preference is visible
	 * @return field exists
	 */
	public boolean checkDrugrefInteractionPreference() {
		log.info("Running method checkDrugrefInteractionPreference...");
		if(driver.findElement(By.id("rxInteractionWarningLevel")) != null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks mandatory OMD Field Requirements
	 * @return fields exist
	 */
	public boolean omdFieldRequirements() {
		log.info("Running method omdFieldRequirements...");
		driver.findElement(By.cssSelector("div#contactInformation ul li:nth-child(1) span.label")).getText().contains("Phone(H)");
		driver.findElement(By.cssSelector("div#contactInformation ul li:nth-child(2) span.label")).getText().contains("Phone(W)");
		driver.findElement(By.cssSelector("div#contactInformation ul li:nth-child(3) span.label")).getText().contains("Cell Phone");
		driver.findElement(By.cssSelector("div#contactInformation ul li:nth-child(4) span.label")).getText().contains("Address");
		driver.findElement(By.cssSelector("div#contactInformation ul li:nth-child(5) span.label")).getText().contains("City");
		driver.findElement(By.cssSelector("div#contactInformation ul li:nth-child(6) span.label")).getText().contains("Province");
		driver.findElement(By.cssSelector("div#contactInformation ul li:nth-child(7) span.label")).getText().contains("Postal");
		driver.findElement(By.cssSelector("div#contactInformation ul li:nth-child(8) span.label")).getText().contains("Email");
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading PatientDemographicPage...");
		parent.get();
		((PatientSearchPage)parent).patientDemographicSearch(Config.getPatientOneDemographicDetails().getLastName());
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if PatientDemographicPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Patient Demographic Page failed to load: URL does not match", url.contains("displaymode=edit"));
		PageUtils.waitForElementToLoad(By.id("editBtn"), driver);
	}
}
