package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.PageUtils;
import org.oscarehr.selenium.resources.ScheduleSettings;

/**
 * The Schedule Provider Page. Appointment Access -> Admin -> Schedule Setting -> Select a provider
 * The associated JSP page is /schedule/scheduletemplateapplying.jsp
 */
public class ScheduleProviderPage extends LoadableComponent<ScheduleProviderPage> {
	private static Logger log = Logger.getLogger(ScheduleProviderPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public ScheduleProviderPage(WebDriver driver, ScheduleSettingMenuPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempts to schedule a provider with the associated schedule settings
	 * @param scheduleSettings The schedule settings
	 * @return The success of the scheduling
	 */
	public boolean scheduleProvider(ScheduleSettings scheduleSettings) {
		log.info("Running method scheduleProvider...");
		String[] date = null;
				
		Select select = new Select(driver.findElement(By.name("mytemplate")));
		if(!PageUtils.checkOption(select, "|" + scheduleSettings.getSummary())) {
			return false;
		}
		select.selectByVisibleText("|" + scheduleSettings.getSummary());
			
		date = scheduleSettings.getStartDate().split("-");
		driver.findElement(By.name("syear")).sendKeys(date[0]);
		driver.findElement(By.name("smonth")).sendKeys(date[1]);
		driver.findElement(By.name("sday")).sendKeys(date[2]);
			
		date = scheduleSettings.getEndDate().split("-");
		driver.findElement(By.name("eyear")).sendKeys(date[0]);
		driver.findElement(By.name("emonth")).sendKeys(date[1]);
		driver.findElement(By.name("eday")).sendKeys(date[2]);
			
		driver.findElement(By.name("checkmon")).click();
		driver.findElement(By.name("checktue")).click();
		driver.findElement(By.name("checkwed")).click();
		driver.findElement(By.name("checkthu")).click();
		driver.findElement(By.name("checkfri")).click();
			
		driver.findElement(By.name("Submit")).submit();
		driver.findElement(By.name("Submit")).submit();
		
		return true;
	}
	
	/**
	 * Attempts to delete an existing schedule with the associated schedule settings
	 * @param scheduleSettings The schedule settings
	 * @return The success of the deletion of the schedule
	 */
	public boolean deleteSchedule(ScheduleSettings scheduleSettings) {
		log.info("Running method deleteSchedule...");
		Select select = new Select(driver.findElement(By.name("select")));
		if(!PageUtils.checkOption(select, scheduleSettings.getStartDate())) {
			return false;
		}
		select.selectByValue(scheduleSettings.getStartDate());
			
		driver.findElement(By.name("command")).click();
	    driver.switchTo().alert().accept();
	    
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading ScheduleProviderPage...");
		parent.get();
		((ScheduleSettingMenuPage)parent).scheduleProviderPage(Config.getScheduleSettings().getProviderId());
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if ScheduleProviderPage is loaded...");
	    String url = driver.getCurrentUrl();
	    assertTrue("Schedule Provider Page failed to load: URL does not match", url.contains("/schedule/scheduletemplateapplying"));
	    PageUtils.waitForElementToLoad(By.name("command"), driver);
	}
}
