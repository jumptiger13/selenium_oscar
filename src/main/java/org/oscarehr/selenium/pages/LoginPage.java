/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.oscarehr.selenium.resources.ConnectionConfig;
import org.oscarehr.selenium.resources.LoginCredentials;
import org.oscarehr.selenium.resources.PageUtils;

import static org.junit.Assert.assertTrue;

/**
 * The Login Page.
 * The associated JSP page is /index.jsp
 */
public class LoginPage extends LoadableComponent<LoginPage> {	
	private static Logger log = Logger.getLogger(LoginPage.class);
	private final WebDriver driver;

	/**
	 * Inherits the web driver to be used
	 * @param driver The web driver to be used
	 * @throws Exception
	 */
	public LoginPage(WebDriver driver) throws Exception {
		this.driver = driver;
	}
	
	/**
	 * Generic login method
	 * @param creds Login Credentials to be used
	 * @return The Success of the login method
	 */
	public boolean login(LoginCredentials creds){
		log.info("Running method login...");
	    driver.findElement(By.name("username")).sendKeys(creds.getUsername());
	    driver.findElement(By.name("password")).sendKeys(creds.getPassword());
	    driver.findElement(By.name("pin")).sendKeys(creds.getPin());
	    driver.findElement(By.cssSelector("form input[type=submit]")).click();
	    String url = driver.getCurrentUrl();
	    return(url.contains("/provider/providercontrol"));
	}

	/**
	 * Attempts to load the page using the base URL.
	 */
	@Override
	protected void load() {
		log.info("loading LoginPage...");
		driver.get(ConnectionConfig.getUrlBase() + "/index.jsp");
	}

	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if LoginPage is loaded...");
		String url = driver.getCurrentUrl();
	    assertTrue("Login Page failed to load: URL does not match", url.contains("/index"));
	    PageUtils.waitForElementToLoad(By.name("username"), driver);
	}
}