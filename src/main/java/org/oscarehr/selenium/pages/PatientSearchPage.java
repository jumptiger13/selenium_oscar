/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.oscarehr.selenium.resources.PageUtils;

/**
 * The Patient Search Page. Appointment Access -> Search
 * The associated JSP page is /demographic/search.jsp
 */
public class PatientSearchPage extends LoadableComponent<PatientSearchPage> {
	private static Logger log = Logger.getLogger(PatientSearchPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public PatientSearchPage(WebDriver driver, AppointmentAccessPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Navigates to the Patient Demographic Edit Page
	 * @return page loaded
	 */
	public boolean patientDemographicSearch(String lastName) {
		log.info("Running method patientDemographicSearch...");
		driver.findElement(By.name("keyword")).sendKeys(lastName);
		driver.findElement(By.name("displaymode")).submit();
		
		PageUtils.waitForElementToLoad(By.xpath("/html/body/div[2]/table/tbody/tr[2]/td/a"), driver);
		driver.findElement(By.xpath("/html/body/div[2]/table/tbody/tr[2]/td/a")).click();
		
		PageUtils.switchWindows("PATIENT DETAIL INFO", driver);
		String url = driver.getCurrentUrl();

		return (url.contains("displaymode=edit"));
	}
	
	/**
	 * Navigates to the Patient EChart Page
	 * @return page loaded
	 */
	public boolean patientEChartSearch(String lastName) {
		log.info("Running method patientEchartSearch...");
		driver.findElement(By.name("keyword")).sendKeys(lastName);
		driver.findElement(By.name("displaymode")).submit();
		
		PageUtils.waitForElementToLoad(By.cssSelector("a.encounterBtn"), driver);
		driver.findElement(By.cssSelector("a.encounterBtn")).click();
		
		PageUtils.switchWindows("Encounter", driver);
		String url = driver.getCurrentUrl();

		return (url.contains("/casemgmt/forward"));
	}
	
	/**
	 * Navigates to the Patient Rx Page
	 * @return page loaded
	 */
	public boolean patientRxSearch(String lastName) {
		log.info("Running method patientRxSearch...");
		driver.findElement(By.name("keyword")).sendKeys(lastName);
		driver.findElement(By.name("displaymode")).submit();
		
		PageUtils.waitForElementToLoad(By.cssSelector("a.rxBtn"), driver);
		driver.findElement(By.cssSelector("a.rxBtn")).click();
		
		PageUtils.switchWindows("Search for Drug", driver);
		String url = driver.getCurrentUrl();

		return (url.contains("/oscarRx/choosePatient"));
	}
	
	/**
	 * Navigates to the Demographic Creation Page
	 * @return page loaded
	 */
	public boolean createPatientDemographicPage(String lastName) {
		log.info("Running method createPatientDemographic...");
		driver.findElement(By.name("keyword")).sendKeys(lastName);
		driver.findElement(By.name("displaymode")).submit();
	
		PageUtils.waitForElementToLoad(By.cssSelector("div.createNew a"), driver);
		driver.findElement(By.cssSelector("div.createNew a")).click();
			
		PageUtils.switchWindows("ADD A DEMOGRAPHIC", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("demographic/demographicaddarecordhtm"));
	}
	
	/**
	 * Check if a patient exists
	 * @return patientExists if the patient exists
	 */
	public boolean checkPatient(String lastName) {
		log.info("Running method checkPatient...");
		boolean patientExists = false;
		driver.findElement(By.name("keyword")).sendKeys(lastName);
		driver.findElement(By.name("displaymode")).submit();
	
		PageUtils.waitForElementToLoad(By.cssSelector("div.createNew a"), driver);
		
		if(!driver.findElements(By.cssSelector("tr.odd")).isEmpty()) {
			patientExists = true;
		}
		return patientExists;
	}
	
	/**
	 * Searches for a patient by health card number
	 */
	public boolean patientHealthCardSearch(String healthCardNumber) {
		log.info("Running method patientHealthCardSearch...");
		Select select = new Select(driver.findElement(By.name("search_mode")));
		select.selectByValue("search_hin");
		
		driver.findElement(By.name("keyword")).sendKeys(healthCardNumber);
		driver.findElement(By.name("displaymode")).submit();
		
		PageUtils.waitForElementToLoad(By.cssSelector("a.encounterBtn"), driver);
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading PatientSearchPage...");
		parent.get();
		((AppointmentAccessPage)parent).patientSearchPage();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if PatientSearchPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Patient Search Page failed to load: URL does not match", url.contains("/demographic/search"));
		PageUtils.waitForElementToLoad(By.name("keyword"), driver);
	}
}
