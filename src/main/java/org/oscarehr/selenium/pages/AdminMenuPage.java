/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.oscarehr.selenium.resources.PageUtils;

/**
 * The Admin Menu Page. Appointment Access -> Admin
 * The associated JSP page is /admin/admin.jsp
 */
public class AdminMenuPage extends LoadableComponent<AdminMenuPage> {
	private static Logger log = Logger.getLogger(AdminMenuPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public AdminMenuPage(WebDriver driver, AppointmentAccessPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Navigates to the Create Provider Record Page
	 * @return page loaded
	 */
	public boolean createProviderRecord() {
		log.info("running method createProviderRecord...");
    	driver.findElement(By.partialLinkText("Add a Provider")).click();
    	
    	PageUtils.switchWindows("ADD A PROVIDER", driver);
    	String url = driver.getCurrentUrl();
    	return (url.contains("/admin/provideraddarecordhtm"));
	}
	
	/**
	 * Navigates to the Search/Edit/Remove Provider Record Page
	 * @return page loaded
	 */
	public boolean editProviderRecord() {
		log.info("running method editProviderRecord...");
		driver.findElement(By.partialLinkText("Search/Edit/Delete Provider")).click();
		
		PageUtils.switchWindows("PROVIDER RECORDS", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/admin/providersearchrecordshtm"));
	}
	
	/**
	 * Navigates to the Schedule Setting Page
	 * @return page loaded
	 */
	public boolean scheduleSetting() {
		log.info("running method scheduleSetting...");
		driver.findElement(By.partialLinkText("Schedule Setting")).click();
		
		PageUtils.switchWindows("SCHEDULE SETTING", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/schedule/scheduletemplatesetting"));
	}
	
	/**
	 * Navigates to the Security Log Report Page
	 * @return page loaded
	 */
	public boolean securityLogReportPage() {
		log.info("running method securityLogReportPage...");
		driver.findElement(By.partialLinkText("Security Log Report")).click();
		
		PageUtils.switchWindows("Log Report", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/admin/logReport"));
	}
	
	/**
	 * Navigates to the Add a Login Record Page
	 * @return page loaded
	 */
	public boolean addLoginRecordPage() {
		log.info("running method addLoginRecordPage...");
		driver.findElement(By.partialLinkText("Login Record")).click();
		
		PageUtils.switchWindows("Add a Login User", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/admin/securityaddarecord"));
	}
	
	/**
	 * Navigates to the Role Creation Page
	 * @return page loaded
	 */
	public boolean roleCreationPage() {
		log.info("running method roleCreationPage...");
		driver.findElement(By.partialLinkText("Add A Role")).click();
		
		PageUtils.switchWindows("Add/Edit Role", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/admin/providerAddRole"));
	}
	
	/**
	 * Navigates to the Assign Role to Provider Page
	 * @return page loaded
	 */
	public boolean roleProviderPage() {
		log.info("running method roleProviderPage...");
		driver.findElement(By.partialLinkText("Assign Role to Provider")).click();
		
		PageUtils.switchWindowsByUrl("/admin/providerRole", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/admin/providerRole"));
	}
	
	/**
	 * Navigates to the Assign Role/Rights to Objects Page
	 * @return page loaded
	 */
	public boolean roleObjectPage() {
		log.info("running method roleObjectPage...");
		driver.findElement(By.partialLinkText("Rights to Object")).click();
		
		PageUtils.switchWindowsByUrl("/admin/providerPrivilege", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/admin/providerPrivilege"));
	}
	
	/**
	 * Navigates to the Query By Example Page
	 * @return page loaded
	 */
	public boolean queryByExamplePage() {
		log.info("running method queryByExamplePage...");
		driver.findElement(By.partialLinkText("Query By Example")).click();
		
		PageUtils.switchWindowsByUrl("/oscarReport/RptByExample", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/oscarReport/RptByExample"));
	}
	
	/**
	 * Navigates to the Manage Facilities Page
	 * @return page loaded
	 */
	public boolean manageFacilitiesPage() {
		log.info("running method manageFacilitiesPage...");
		driver.findElement(By.partialLinkText("Manage Facilities")).click();
		
		PageUtils.switchWindowsByUrl("/FacilityManager", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/FacilityManager"));
	}
	
	/**
	 * Navigates to the Create Flowsheet Page
	 * @return page loaded
	 */
	public boolean createFlowsheetPage() {
		log.info("running method createFlowsheetPage...");
		driver.findElement(By.partialLinkText("Create New Flowsheet")).click();
		
		PageUtils.switchWindowsByUrl("/oscarMeasurements/adminFlowsheet", driver);
		String url = driver.getCurrentUrl();
		return (url.contains("/oscarMeasurements/adminFlowsheet"));
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading AdminMenuPage...");
		parent.get();
		((AppointmentAccessPage)parent).adminPage();
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if AdminMenuPage is loaded...");
	    String url = driver.getCurrentUrl();
	    assertTrue("Admin Menu Page failed to load: URL does not match", url.contains("/admin/admin"));
	    PageUtils.waitForElementToLoad(By.partialLinkText("Add a Provider"), driver);
	}
}
