/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.pages;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.oscarehr.selenium.resources.AppointmentDetails;
import org.oscarehr.selenium.resources.Config;
import org.oscarehr.selenium.resources.PageUtils;

/**
 * The Appointment Management Page. Appointment Access -> Appointment Management
 * The associated JSP page is /appointment/appointmentcontrol.jsp
 */
public class AppointmentManagementPage extends LoadableComponent<AppointmentManagementPage> {
	private static Logger log = Logger.getLogger(AppointmentManagementPage.class);
	private final WebDriver driver;
	private final LoadableComponent<?> parent;
	
	/**
	 * References the driver to be used and the parent page to be inherited from
	 * @param driver The web driver to be used
	 * @param parent The parent page which this page inherits control from
	 */
	public AppointmentManagementPage(WebDriver driver, AppointmentAccessPage parent) {
		this.driver = driver;
		this.parent = parent;
	}
	
	/**
	 * Attempts to delete an existing appointment
	 * @return The success of the deletion
	 */
	public boolean deleteAppointment() {
    	driver.findElement(By.id("deleteButton")).click();
    	driver.switchTo().alert().accept();
    	return true;
	}
	
	/**
	 * Edits an appointment with the associated appointment details
	 * @param appDetails The appointment details to be inserted into the existing appointment
	 * @return The success of the modification
	 */
	public boolean editAppointment(AppointmentDetails appDetails) {
		log.info("Running method editAppointment...");
    	//TODO: add a name or id to the search button element
		/*driver.findElement(By.name("searchBtn")).click();
    	
    	PageUtils.switchWindows("PATIENT SEARCH", driver);
			    
	    driver.findElement(By.name("keyword")).sendKeys(appDetails.getLastName());
	    driver.findElement(By.name("displaymode")).submit();
	    
	    WebDriverWait wait = new WebDriverWait(driver, 5);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='searchResults']/ul/form/li[2]/div[3]")));
    	driver.findElement(By.xpath("//div[@id='searchResults']/ul/form/li[2]/div[3]")).click();*/
    	
		driver.findElement(By.name("reason")).clear();
    	driver.findElement(By.name("notes")).clear();
    	driver.findElement(By.name("location")).clear();
    	driver.findElement(By.name("resources")).clear();
		
    	//Fill in reason, notes, location, and resources with test data and submit the appointment creation
    	driver.findElement(By.name("reason")).sendKeys(appDetails.getReason());
    	driver.findElement(By.name("notes")).sendKeys(appDetails.getNotes());
    	driver.findElement(By.name("location")).sendKeys(appDetails.getLocation());
    	driver.findElement(By.name("resources")).sendKeys(appDetails.getResources());
    	driver.findElement(By.id("updateButton")).click();
    	
		return true;
	}
	
	/**
	 * Attempts to load the page navigating to itself from the parent page.
	 */
	@Override
	protected void load() {
		log.info("loading AppointmentManagementPage...");
		parent.get();
		((AppointmentAccessPage)parent).appointmentManagementPage(Config.getAppointmentOneDetails().getLastName());
	}
	
	/**
	 * Verifies that the page is loaded. Checks for the correct URL then checks for an element on the page.
	 */
	@Override
	protected void isLoaded() throws Error {
		log.info("checking if AppointmentManagementPage is loaded...");
		String url = driver.getCurrentUrl();
		assertTrue("Appointment Management Page failed to load: URL does not match", url.contains("/appointment/appointmentcontrol"));
		PageUtils.waitForElementToLoad(By.name("reason"), driver);
	}
}
