package org.oscarehr.selenium.support;

import org.apache.commons.io.FileUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.oscarehr.selenium.resources.PageUtils;

import java.io.File;
import java.io.IOException;

/**
 * This JUnit Rule latches on to the Web Driver and acts as a test watcher
 */
public class ScreenshotRule extends TestWatcher {
    private WebDriver driver;

    /**
     * Generic Construct
     * @param driver The web driver that the rule latches on to
     */
    public ScreenshotRule(WebDriver driver) {
        this.driver =  driver;
    }

    /**
     * This method is executed when a test fails in which case it will take a screenshot and save the screenshot as a file
     * @throws Exception
     * @param description The information of the current method being executed
     */
    @Override
    protected void failed(Throwable e, Description description) {
        TakesScreenshot takesScreenshot = (TakesScreenshot)driver;

        File scrFile = takesScreenshot.getScreenshotAs(OutputType.FILE);
        File destFile = getDestinationFile(description);
        try {
            FileUtils.copyFile(scrFile, destFile);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }
    
    /**
     * This methods occurs when a test has finished (successful or not) and attempts to log out
     * @param description The information of the current method being executed
     */
    @Override
    protected void finished(Description description) {
    	PageUtils.closeAllWindows(driver);
    	PageUtils.logout(driver);
    }

    /**
     * This method creates and saves the screenshot
     * @param description The information of the current method being executed
     * @return absoluteFileName The absolute file path for the screenshot
     */
    private File getDestinationFile(Description description) {
        String fileName = description.getClassName() + "." + description.getMethodName() + ".png";
        String absoluteFileName = "target/surefire-reports/failed-test-screenshots/" + fileName;

        return new File(absoluteFileName);
    }
}
