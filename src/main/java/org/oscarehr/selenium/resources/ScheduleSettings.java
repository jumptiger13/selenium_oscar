package org.oscarehr.selenium.resources;

public class ScheduleSettings {
	private String providerId;
	private String name;
	private String summary;
	private String timeCode;
	private String startDate;
	private String endDate;
	
	public ScheduleSettings() {	}
	
	/**
	 * @param providerId The provider's first and last name in the format "last name, first name" 
	 * @param name The name of the schedule
	 * @param summary The summary of the schedule
	 * @param timeCode The time code to be inserted (e.g. 1 = 15 minute appointment)
	 * @param startDate The start date of the schedule (e.g. 2013-01-01)
	 * @param endDate The end date of the schedule (e.g. 2013-01-31)
	 */
	public ScheduleSettings(String providerId, String name, String summary, String timeCode, String startDate, String endDate) {
		this.providerId = providerId;
		this.name = name;
		this.summary = summary;
		this.timeCode = timeCode;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getTimeCode() {
		return timeCode;
	}

	public void setTimeCode(String timeCode) {
		this.timeCode = timeCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}
	
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
