/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.resources;

public class AppointmentDetails {
	private String lastName;
	private String reason;
	private String notes;
	private String location;
	private String resources;
	
	public AppointmentDetails() { }
	
	/**
	 * @param lastName the last name of the patient to be added to the appointment
	 * @param reason the reason for the appointment
	 * @param notes notes for the appointment
	 * @param location location of the appointment
	 * @param resources any resources required/used for the appointment
	 */	
	public AppointmentDetails(String lastName, String reason, String notes, String location, String resources) {
		this.lastName = lastName;
		this.reason = reason;
		this.notes = notes;
		this.location = location;
		this.resources = resources;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String LastName) {
		this.lastName = LastName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String Reason) {
		this.reason = Reason;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String Notes) {
		this.notes = Notes;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String Location) {
		this.location = Location;
	}

	public String getResources() {
		return resources;
	}

	public void setResources(String Resources) {
		this.resources = Resources;
	}
}
