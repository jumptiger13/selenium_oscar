/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.resources;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * This Config is used to establish connection related information for tests such as page timeouts and base URLs
 */
public class ConnectionConfig {
	private static String urlBase;
	private static long pageTimeout;
	
	/**
	 * Default Constructor
	 */
	public ConnectionConfig(){ }	
	
	/**
	 * @return the pageTimeout
	 */
	public static long getPageTimeout() {
		return pageTimeout;
	}
	/**
	 * @param pageTimeout the pageTimeout to set
	 */
	public static void setPageTimeout(long pageTimeout) {
		ConnectionConfig.pageTimeout = pageTimeout;
	}
	/**
	 * @return the urlBase
	 */
	public static String getUrlBase() {
		return urlBase;
	}
	/**
	 * @param urlBase the urlBase to set
	 */
	public static void setUrlBase(String urlBase) {
		ConnectionConfig.urlBase = urlBase;
	}
	
	/**
	 * Populates the Config java bean with the data contained in the XML file
	 * @param filePath the filePath from the home directory
	 * @return the config object
	 * @throws Exception
	 */
	public static ConnectionConfig load(String filePath) throws Exception{
		FileInputStream os = new FileInputStream(filePath);
		XMLDecoder decoder = new XMLDecoder(os);
		ConnectionConfig cfg = (ConnectionConfig)decoder.readObject();
		decoder.close();
		return cfg;
	}
	
	/**
	 * Saves the Config java bean information into the XML file
	 * @param filePath the filePath from the home directory
	 * @return the success of the save method
	 * @throws Exception
	 */
	public boolean save(String filePath) throws Exception{
		FileOutputStream os = new FileOutputStream(filePath);
		XMLEncoder encoder = new XMLEncoder(os);
		encoder.writeObject(this);
		encoder.close();
		return true;
	}
}
