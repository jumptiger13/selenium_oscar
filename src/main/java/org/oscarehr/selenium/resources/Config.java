/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.resources;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * A java bean that stores all the necessary information such as demographic details, appointment details, prescription details, etc.
 */
public class Config {
	private static LoginCredentials goodCredentials;
	private static LoginCredentials badCredentials;
	private static LoginCredentials noCredentials;
	private static PatientDemographicDetails patientOneDemographicDetails;
	private static AppointmentDetails appointmentOneDetails;
	private static AppointmentDetails appointmentTwoDetails;
	private static EChartNote eChartNoteOne;
	private static RxPrescription rxPrescriptionOne;
	private static ProviderDetails providerOneDetails;
	private static ScheduleSettings scheduleSettings;

	/**
	 * Default Constructor
	 */
	public Config(){ }
	
	/**
	 * @return the scheduleSettings
	 */
	public static ScheduleSettings getScheduleSettings() {
		return scheduleSettings;
	}
	/**
	 * @param scheduleSettings the scheduleSettings to set
	 */
	public static void setScheduleSettings(ScheduleSettings scheduleSettings) {
		Config.scheduleSettings = scheduleSettings;
	}
	/**
	 * @return the providerOneDetails
	 */
	public static ProviderDetails getProviderOneDetails() {
		return providerOneDetails;
	}
	/**
	 * @param providerOneDetails the providerOneDetails to set
	 */
	public static void setProviderOneDetails(ProviderDetails providerOneDetails) {
		Config.providerOneDetails = providerOneDetails;
	}
	/**
	 * @return the rxPrescriptionOne
	 */
	public static RxPrescription getRxPrescriptionOne() {
		return rxPrescriptionOne;
	}
	/**
	 * @param rxPrescriptionOne the rxPrescriptionOne to set
	 */
	public static void setRxPrescriptionOne(RxPrescription rxPrescriptionOne) {
		Config.rxPrescriptionOne = rxPrescriptionOne;
	}
	/**
	 * @return the eChartNoteOne
	 */
	public static EChartNote getEChartNoteOne() {
		return eChartNoteOne;
	}
	/**
	 * @param eChartNoteOne the eChartNoteOne to set
	 */
	public static void setEChartNoteOne(EChartNote eChartNoteOne) {
		Config.eChartNoteOne = eChartNoteOne;
	}
	/**
	 * @return the appointmentTwoDetails
	 */
	public static AppointmentDetails getAppointmentTwoDetails() {
		return appointmentTwoDetails;
	}
	/**
	 * @param appointmentTwoDetails the appointmentTwoDetails to set
	 */
	public static void setAppointmentTwoDetails(AppointmentDetails appointmentTwoDetails) {
		Config.appointmentTwoDetails = appointmentTwoDetails;
	}
	/**
	 * @return the appointmentOneDetails
	 */
	public static AppointmentDetails getAppointmentOneDetails() {
		return appointmentOneDetails;
	}
	/**
	 * @param appointmentOneDetails the appointmentOneDetails to set
	 */
	public static void setAppointmentOneDetails(AppointmentDetails appointmentOneDetails) {
		Config.appointmentOneDetails = appointmentOneDetails;
	}
	/**
	 * @return the patientOneDemographicDetails
	 */
	public static PatientDemographicDetails getPatientOneDemographicDetails() {
		return patientOneDemographicDetails;
	}
	/**
	 * @param patientOneDemographicDetails the patientOneDemographicDetails to set
	 */
	public static void setPatientOneDemographicDetails(PatientDemographicDetails patientOneDemographicDetails) {
		Config.patientOneDemographicDetails = patientOneDemographicDetails;
	}
	/**
	 * @return the goodCredentials
	 */
	public static LoginCredentials getGoodCredentials() {
		return goodCredentials;
	}
	/**
	 * @param goodCredentials the goodCredentials to set
	 */
	public static void setGoodCredentials(LoginCredentials goodCredentials) {
		Config.goodCredentials = goodCredentials;
	}
	/**
	 * @return the badCredentials
	 */
	public static LoginCredentials getBadCredentials() {
		return badCredentials;
	}
	/**
	 * @param badCredentials the badCredentials to set
	 */
	public static void setBadCredentials(LoginCredentials badCredentials) {
		Config.badCredentials = badCredentials;
	}
	/**
	 * @return the noCredentials
	 */
	public static LoginCredentials getNoCredentials() {
		return noCredentials;
	}
	/**
	 * @param noCredentials the noCredentials to set
	 */
	public static void setNoCredentials(LoginCredentials noCredentials) {
		Config.noCredentials = noCredentials;
	}
	
	/**
	 * Populates the Config java bean with the data contained in the XML file
	 * @param filePath the filePath from the home directory
	 * @return the config object
	 * @throws Exception
	 */
	public static Config load(String filePath) throws Exception{
		FileInputStream os = new FileInputStream(filePath);
		XMLDecoder decoder = new XMLDecoder(os);
		Config cfg = (Config)decoder.readObject();
		decoder.close();
		return cfg;
	}
	
	/**
	 * Saves the Config java bean information into the XML file
	 * @param filePath the filePath from the home directory
	 * @return the success of the save method
	 * @throws Exception
	 */
	public boolean save(String filePath) throws Exception{
		FileOutputStream os = new FileOutputStream(filePath);
		XMLEncoder encoder = new XMLEncoder(os);
		encoder.writeObject(this);
		encoder.close();
		return true;
	}
	
}
