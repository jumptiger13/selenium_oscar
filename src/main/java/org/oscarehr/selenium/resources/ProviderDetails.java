/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.resources;

public class ProviderDetails {
	private String lastName;
	private String firstName;
	private String providerType;
	private String ohipNo;
	private String sex;
	private String dateOfBirth;
	private String address;
	private String phone;
	private String workphone;
	private String email;
	
	public ProviderDetails() { }
	
	/**
	 * @param lastName The last name of the provider
	 * @param firstName The first name of the provider
	 * @param providerType The provider type of the provider (e.g. doctor, nurse)
	 * @param ohipNo The OHIP number of the provider
	 * @param sex The gender of the provider
	 * @param dateOfBirth The date of birth of the provider (e.g. 2013-01-01)
	 * @param address The home address of the provider
	 * @param phone The home phone of the provider
	 * @param workphone The work phone of the provider
	 * @param email The provider's email
	 */	
	public ProviderDetails(String lastName, String firstName, String providerType, String ohipNo, String sex, 
						    String dateOfBirth, String address, String phone, String workphone, String email) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.providerType = providerType;
		this.ohipNo = ohipNo;
		this.sex = sex;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.phone = phone;
		this.workphone = workphone;
		this.email = email;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getProviderType() {
		return providerType;
	}
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}
	public String getOhipNo() {
		return ohipNo;
	}
	public void setOhipNo(String ohipNo) {
		this.ohipNo = ohipNo;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getWorkphone() {
		return workphone;
	}
	public void setWorkphone(String workphone) {
		this.workphone = workphone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
