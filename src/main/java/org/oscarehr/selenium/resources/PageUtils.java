/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.resources;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.oscarehr.selenium.resources.ConnectionConfig;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.FluentWait;

/**
 * This class stores generic page utilities such as switching windows or checking if elements exist on a page.
 * All functions in this class contain no direct references to page objects.
 */
public class PageUtils {
	private static Logger log = Logger.getLogger(PageUtils.class);
	private static WebDriverWait wait;
	
	/**
	 * Switches active window to window with given title.
	 * @param windowTitle The name of the window to switch to
	 * @param driver The WebDriver to switch windows in
	 */
	public static void switchWindows(String windowTitle, WebDriver driver){
		WebDriver mainPage = null;
		Iterator<String> windowIterator = driver.getWindowHandles().iterator();
		while(windowIterator.hasNext()) { 
			String windowHandle = windowIterator.next(); 
			mainPage = driver.switchTo().window(windowHandle);
			if (mainPage.getTitle().contains(windowTitle)) {
				break;
			}
		}
	}
	
	/**
	 * Switches active window to window with given Url.
	 * @param url A part of the url of the window to switch to
	 * @param driver The WebDriver to switch windows in
	 */
	public static void switchWindowsByUrl(String url, WebDriver driver){
		WebDriver mainPage = null;
		Iterator<String> windowIterator = driver.getWindowHandles().iterator();
		while(windowIterator.hasNext()) { 
			String windowHandle = windowIterator.next(); 
			mainPage = driver.switchTo().window(windowHandle);
			if (mainPage.getCurrentUrl().contains(url)) {
				break;
			}
		}
	}
	
	/**
	 * Closes all open windows except for the appointment access page
	 * @param driver The WebDriver to close windows from
	 */
	public static void closeAllWindows(WebDriver driver){
		WebDriver mainPage = null;
		//We need to account for the possibility of a 'phantom window handle' due to refreshing pages
		if(driver.getWindowHandles().size() > 2) {
			Iterator<String> windowIterator = driver.getWindowHandles().iterator();
			while(windowIterator.hasNext()) { 
				String windowHandle = windowIterator.next();
				mainPage = driver.switchTo().window(windowHandle);
				if (!mainPage.getCurrentUrl().contains("/provider/providercontrol")) {
					driver.close();
				}
			}
		}
	}
	
	/**
	 * Switches active window to the Appointment Access page and attempts to logout.
	 * @param driver The WebDriver to switch windows in
	 */
	public static void logout(WebDriver driver){
		try {
			switchWindows("Appointment Access", driver);
			driver.get(ConnectionConfig.getUrlBase() + "/logout.jsp");
		} catch(Exception e) {
			log.error("Failed to gracefully log out", e);
		}
	}
	
	/**
	 * Waits for an element on the current page to load.
	 * @param locator The By locator to look for the element (locator type and value)
	 * @param driver The WebDriver where the element exists
	 */
	public static void waitForElementToLoad(By locator, WebDriver driver) {
		try {
			wait = new WebDriverWait(driver, ConnectionConfig.getPageTimeout());
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		} catch(Exception e) {
			log.error("Failed to find locator: " + locator, e);
		}
	}
	
	/**
	 * Waits for an element on the current page to load and be enabled.
	 * @param locator The By locator to look for the element (locator type and value)
	 * @param driver The WebDriver where the element exists
	 */
	public static void waitForElementToEnable(By locator, WebDriver driver) {
		try {
			wait = new WebDriverWait(driver, ConnectionConfig.getPageTimeout());
			wait.until(ExpectedConditions.elementToBeClickable(locator));
		} catch(Exception e) {
			log.error("Failed to find locator: " + locator, e);
		}
	}
	
	/**
	 * Waits for an element to contain a certain value.
	 * @param locator The By locator to look for the element (locator type and value)
	 * @param value The value to look for in the element
	 * @param driver The WebDriver where the element exists
	 */
	public static void waitForElementToContainValue(By locator, String value, WebDriver driver) {
		try {
			wait = new WebDriverWait(driver, ConnectionConfig.getPageTimeout());
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		} catch(Exception e) {
			log.error("Failed to find locator: " + locator + " containing value " + value, e);
		}
	}
	
	/**
	 * Searches for an alert window for 5 seconds and accepts it if one exists otherwise continues.
	 * @param driver The WebDriver to search for the alert
	 */
	public static void confirmAlertWindow(WebDriver driver) {
		Wait<WebDriver> fluentWait = new FluentWait<WebDriver>(driver)
				.withTimeout(5, TimeUnit.SECONDS)
				.ignoring(TimeoutException.class);
		try {
			Alert alert = fluentWait.until(ExpectedConditions.alertIsPresent());
			if(alert.getText() != null) {
				alert.accept();
			}
		} catch(WebDriverException e) {
			log.info("ConfirmAlertWindow: no alert present to close");
		}
	}
	
	/**
	 * Searches for an alert window for 5 seconds and dismisses it if one exists otherwise continues.
	 * @param driver The WebDriver to search for the alert
	 */
	public static void dismissAlertWindow(WebDriver driver) {
		Wait<WebDriver> fluentWait = new FluentWait<WebDriver>(driver)
				.withTimeout(5, TimeUnit.SECONDS)
				.ignoring(TimeoutException.class);
		try {
			Alert alert = fluentWait.until(ExpectedConditions.alertIsPresent());
			if(alert.getText() != null) {
				alert.dismiss();
			}
		} catch(WebDriverException e) {
			log.info("DismissAlertWindow: no alert present to close");
		}
	}
	
	/**
	 * Searches for an alert window for 10 seconds and if it exists log the message.
	 * @param driver The WebDriver to search for the alert
	 */
	public static void getOtherWindow(WebDriver driver, String originalHandle) {
		long timeoutEnd = System.currentTimeMillis() + 10000;
		while (System.currentTimeMillis() < timeoutEnd) {
			if(driver.getWindowHandles().size() == 1 ) {
				try {
					Thread.sleep(100);
				} catch(Exception e) {
					
				}
			} else {
				log.info("More than one window is present");
				break;
			}
		}
		
		for (String handle : driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				driver.switchTo().window(handle);
				log.info("window handle:" + driver.getWindowHandle());
				log.info("window title:" + driver.getTitle());
				log.info("window URL:" + driver.getCurrentUrl());
				break;
			}
		}
	}
	
	/**
	 * Checks if an option exists in a given select box
	 * @param select The select box to be checked
	 * @param text The text or part of the text of the option to be checked
	 */
	public static boolean checkOption(Select select, String text) {
		Iterator<WebElement> optionIterator = select.getOptions().iterator();
		while(optionIterator.hasNext()) {
			WebElement option = optionIterator.next();
			if(option.getText().contains(text)) {
				return true;
			}
		}
		return false;
	}
}
