/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.resources;

public class RxPrescription {
	private String prescriptionName;
	private String instructions;
	private String quantity;
	private String repeats;
	private String method;
	private String route;
	private String frequency;
	private String minimum;
	private String maximum;
	private String duration;
	private String durationUnit;
	private String quantityStr;
	private String additionalNotes;

	public RxPrescription() { }
	
	/**
	 * @param prescriptionName The prescription name
	 * @param instructions The prescription instructions for usage
	 * @param quantity The quantity of the prescription
	 * @param repeats The amount of repeats for the prescription
	 * @param method The act of applying the drug (e.g. take, rub)
	 * @param route The route of absorbing the drug (e.g. oral, topical)
	 * @param frequency The time frame frequency for drug dosage
	 * @param minimum The minimum quantity of the drug
	 * @param maximum The maximum quantity of the drug
	 * @param duration The duration of the prescription
	 * @param durationUnit The unit for the duration of the prescription (e.g. day, week, month)
	 * @param quantityStr The String equivalent of the quantity
	 */
	public RxPrescription( String prescriptionName, String instructions, String quantity, String repeats, String method, String route, String frequency,
			  String minimum, String maximum, String duration, String durationUnit, String quantityStr) {
		this.prescriptionName = prescriptionName;
		this.instructions = instructions;
		this.quantity = quantity;
		this.repeats = repeats;
		this.method = method;
		this.route = route;
		this.frequency = frequency;
		this.minimum = minimum;
		this.maximum = maximum;
		this.duration = duration;
		this.durationUnit = durationUnit;
		this.quantityStr = quantityStr;
	}
	
	public String getPrescriptionName() {
		return prescriptionName;
	}
	public void setPrescriptionName(String prescriptionName) {
		this.prescriptionName = prescriptionName;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getRepeats() {
		return repeats;
	}
	public void setRepeats(String repeats) {
		this.repeats = repeats;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getMinimum() {
		return minimum;
	}
	public void setMinimum(String minimum) {
		this.minimum = minimum;
	}
	public String getMaximum() {
		return maximum;
	}
	public void setMaximum(String maximum) {
		this.maximum = maximum;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getDurationUnit() {
		return durationUnit;
	}
	public void setDurationUnit(String durationUnit) {
		this.durationUnit = durationUnit;
	}
	public String getQuantityStr() {
		return quantityStr;
	}
	public void setQuantityStr(String quantityStr) {
		this.quantityStr = quantityStr;
	}
	public String getAdditionalNotes() {
		return additionalNotes;
	}
	public void setAdditionalNotes(String additionalNotes) {
		this.additionalNotes = additionalNotes;
	}
}
