/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.selenium.resources;

public class PatientDemographicDetails {

	private String lastName;
	private String firstName;
	private String yearOfBirth;
	private String hin;
	private String ver;
	private String eff_date;
	private String hc_renew_date;
	private String provider;
	private String rosterStatus;
	private String roster_date;
	
	public PatientDemographicDetails() { }
	
	/**
	 * @param lastName The last name of the patient
	 * @param firstName The first name of the patient
	 * @param yearOfBirth The year of birth of the patient (e.g. 2013-01-01)
	 * @param hin The health card number for the patient
	 * @param ver The version number of the health card for the patient
	 * @param eff_date The effective date of the health card for the patient (e.g. 2013-01-01)
	 * @param hc_renew The expected renewal date of the health card for the patient (e.g. 2013-01-01)
	 * @param provider The attending provider of the patient
	 * @param rosterStatus The roster status of the patient
	 * @param roster_date The roster date of the patient
	 */
	public PatientDemographicDetails( String lastName, String firstName, String yearOfBirth, String hin, String ver, String eff_date, String hc_renew_date,
									  String provider, String rosterStatus, String roster_date ) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.yearOfBirth = yearOfBirth;
		this.hin = hin;
		this.ver = ver;
		this.eff_date = eff_date;
		this.hc_renew_date = hc_renew_date;
		this.provider = provider;
		this.rosterStatus = rosterStatus;
		this.roster_date = roster_date;
	}
	
	public String getHin() {
		return hin;
	}

	public void setHin(String hin) {
		this.hin = hin;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getEff_date() {
		return eff_date;
	}

	public void setEff_date(String eff_date) {
		this.eff_date = eff_date;
	}

	public String getHc_renew_date() {
		return hc_renew_date;
	}

	public void setHc_renew_date(String hc_renew_date) {
		this.hc_renew_date = hc_renew_date;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getRosterStatus() {
		return rosterStatus;
	}

	public void setRosterStatus(String rosterStatus) {
		this.rosterStatus = rosterStatus;
	}

	public String getRoster_date() {
		return roster_date;
	}

	public void setRoster_date(String roster_date) {
		this.roster_date = roster_date;
	}
	
	public void setLastName(String LastName) {
		this.lastName = LastName;
	}
	
	public String getLastName() {
		return this.lastName;
	}
	
	public void setFirstName(String FirstName) {
		this.firstName = FirstName;
	}
	
	public String getFirstName() {
		return this.firstName;
	}
	
	public void setYearOfBirth(String YearOfBirth) {
		this.yearOfBirth = YearOfBirth;
	}
	
	public String getYearOfBirth() {
		return this.yearOfBirth;
	}
}
